# The correct `segment_id` was eyeballed from the Strava URLs
cat musther.json | \
  jq -c '.efforts | 
  [
    .[] | select(.segment_id == 10552667) | {time:.elapsed_time_raw}
  ]' | \
# This next bit is cargoculted magic - I don't know how it works but it does.
  jq -r '
  [ 
    keys[] as $k | .[$k] | .key=$k+1 
  ] | .[] | [ .key, .time ] | @csv' > musther.data
